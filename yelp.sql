sudo /opt/lampp/lampp start
sudo /opt/lampp/lampp status

sudo /opt/lampp/bin/mysql 
sudo /opt/lampp/bin/mysql -u username -p databasename

sudo /opt/lampp/bin/mysql -u bob -p
EO750FAS7F6Tvpzx

sudo xdg-open /

**************************************************************************

This script is for MariaDB

https://www.yelp.com/dataset/documentation/main

**************************************************************************

For MariaDB to talk to Python

1. Install MariaDB Connector/C, which is a dependency.

$ sudo apt-get install libmariadb3 libmariadb-dev

2. Download MariaDB Connector/Pythons source tarball at: https://mariadb.com/downloads/#connectors

3. Extract the source tarball:

$ mkdir mariadb-connector-python
$ cd mariadb-connector-python
$ tar -xvzf ../mariadb-connector-python-*.tar.gz

4. Use PIP to install MariaDB Connector/Python from source:

$ pip3 install ./mariadb-*


https://mariadb.com/docs/appdev/connector-python/

OR....

Installing from PyPI

    Install MariaDB Connector/C, which is a dependency.

    $ sudo apt-get install libmariadb3 libmariadb-dev

    Use PIP to install MariaDB Connector/Python through PyPI:

    $ pip3 install mariadb


**************************************************************************
pip3 install mysql-connector-python

import mysql.connector as mariadb

**************************************************************************

DROP DATABASE yelp;
CREATE DATABASE yelp DEFAULT CHARACTER SET utf8;
USE yelp;
SELECT database();

**************************************************************************


DROP TABLE IF EXISTS friend;
DROP TABLE IF EXISTS elite;
DROP TABLE IF EXISTS hours;
DROP TABLE IF EXISTS category;
DROP TABLE IF EXISTS attribute;
DROP TABLE IF EXISTS tip;
DROP TABLE IF EXISTS checkin;
DROP TABLE IF EXISTS photo;
DROP TABLE IF EXISTS review;
DROP TABLE IF EXISTS business;
DROP TABLE IF EXISTS user;


CREATE TABLE user (
	user_pk VARCHAR(22) PRIMARY KEY,
	name VARCHAR(255),
	review_count INT,
	yelping_since DATETIME,
	useful INT,
	funny INT,
	cool INT,
	fans INT,
    average_stars FLOAT,
    compliment_hot INT,
    compliment_more INT,
    compliment_profile INT,
    compliment_cute INT,
    compliment_list INT,
    compliment_note INT,
    compliment_plain INT,
    compliment_cool INT,
    compliment_funny INT,
    compliment_writer INT,
    compliment_photos INT
) ENGINE = InnoDB;


CREATE TABLE business (
	business_pk VARCHAR(22) PRIMARY KEY,
	name VARCHAR(255),
	address VARCHAR(255),
	city VARCHAR(255),
	state VARCHAR(255),
	postcode VARCHAR(255),
	latitude DOUBLE,
	longitude DOUBLE,
	stars FLOAT,
	review_count INT,
	is_open BOOLEAN
) ENGINE = InnoDB;


CREATE TABLE review (
    review_pk VARCHAR(22) PRIMARY KEY,
    business_fk VARCHAR(22),
    user_fk VARCHAR(22),
    stars INT,
    date DATETIME,
    text TEXT,
    useful INT,
    funny INT,
    cool INT,
	CONSTRAINT business_many_review
	FOREIGN KEY (business_fk) REFERENCES business(business_pk)
	ON DELETE CASCADE
	ON UPDATE RESTRICT,
	CONSTRAINT fk_user
	FOREIGN KEY (user_fk) REFERENCES user(user_pk)
	ON DELETE CASCADE
	ON UPDATE RESTRICT
) ENGINE = InnoDB;


CREATE TABLE checkin (
    business_fk VARCHAR(22),
    date DATETIME,
	CONSTRAINT business_many_checkin
	FOREIGN KEY (business_fk) REFERENCES business(business_pk)
	ON DELETE CASCADE
	ON UPDATE RESTRICT
) ENGINE = InnoDB;


CREATE TABLE tip (
	user_fk VARCHAR(22),
    business_fk VARCHAR(22),
    text TEXT,
    date DATETIME,
    compliment_count INT,
	CONSTRAINT business_many_tip
	FOREIGN KEY (business_fk) REFERENCES business(business_pk)
	ON DELETE CASCADE
	ON UPDATE RESTRICT,
	CONSTRAINT user_many_tip
	FOREIGN KEY (user_fk) REFERENCES user(user_pk)
	ON DELETE CASCADE
	ON UPDATE RESTRICT
) ENGINE = InnoDB;


CREATE TABLE photo (
    photo_pk VARCHAR(22) PRIMARY KEY,
    business_fk VARCHAR(22),
	caption VARCHAR(255),
	label VARCHAR(255),
	CONSTRAINT business_many_photo
	FOREIGN KEY (business_fk) REFERENCES business(business_pk)
	ON DELETE CASCADE
	ON UPDATE RESTRICT
) ENGINE = InnoDB;


CREATE TABLE hours (
    business_fk VARCHAR(22),
    hours VARCHAR(255),
	CONSTRAINT business_many_hours
	FOREIGN KEY (business_fk) REFERENCES business(business_pk)
	ON DELETE CASCADE
	ON UPDATE RESTRICT
) ENGINE = InnoDB;


CREATE TABLE category (
    business_fk VARCHAR(22),
    category VARCHAR(255),
	CONSTRAINT business_many_category
	FOREIGN KEY (business_fk) REFERENCES business(business_pk)
	ON DELETE CASCADE
	ON UPDATE RESTRICT
) ENGINE = InnoDB;


CREATE TABLE attribute (
    business_fk VARCHAR(22),
    name VARCHAR(255),
    value TEXT,
	CONSTRAINT business_many_attribute
	FOREIGN KEY (business_fk) REFERENCES business(business_pk)
	ON DELETE CASCADE
	ON UPDATE RESTRICT
) ENGINE = InnoDB;


CREATE TABLE friend (
    user_fk VARCHAR(22),
    friend_id VARCHAR(22),
	CONSTRAINT user_many_friend
	FOREIGN KEY (user_fk) REFERENCES user(user_pk)
	ON DELETE CASCADE
	ON UPDATE RESTRICT
) ENGINE = InnoDB;


CREATE TABLE elite (
    user_fk VARCHAR(22),
    year YEAR,
	CONSTRAINT user_many_elite
	FOREIGN KEY (user_fk) REFERENCES user(user_pk)
	ON DELETE CASCADE
	ON UPDATE RESTRICT
) ENGINE = InnoDB;

